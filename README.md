App for parental access, balance control, spending and child evolution


## Steps to run project

In the project directory, you can run:

### `yarn` or `npm install`

This will add the dependencies required to run the project

### `yarn start` or `npm start`

This will start the project


