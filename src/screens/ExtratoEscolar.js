import React, { Component } from "react";
import { StyleSheet, View, Text, Image } from "react-native";

function ExtratoEscolar(props) {
  return (
    <View style={styles.container}>
      <View style={styles.rect2}>
        <Text style={styles.titulo}>Progresso escolar</Text>
        <View style={styles.item}>
          <Text style={styles.atividadeConcluida}>Atividade 1 concluída</Text>
          <Image
            source={require("../assets/images/star.png")}
            resizeMode="contain"
            style={styles.image}
          ></Image>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(236,234,234,1)"
  },
  rect2: {
    backgroundColor: "rgba(219,213,213,1)",
    justifyContent: "flex-start",
    flex: 1
  },
  titulo: {
    height: 27,
    color: "rgba(53,52,52,1)",
    alignSelf: "stretch",
    justifyContent: "space-between",
    marginTop: 80,
    fontSize: 22,
    fontFamily: "roboto-regular",
    textAlign: "center"
  },
  item: {
    height: 45,
    backgroundColor: "rgba(231,226,226,1)",
    flexDirection: "row-reverse",
    alignSelf: "stretch",
    justifyContent: "flex-end",
    marginTop: 10,
    marginRight: 30,
    marginBottom: 10,
    marginLeft: 30,
    borderRadius: 5
  },
  atividadeConcluida: {
    color: "rgba(14,14,14,1)",
    alignSelf: "center",
    marginLeft: 10,
    fontSize: 12,
    fontFamily: "roboto-regular"
  },
  image: {
    width: 33,
    height: 34,
    alignSelf: "center",
    marginLeft: 10
  }
});

export default ExtratoEscolar;
