import React, { Component } from "react";
import { StyleSheet, View, Text, Image } from "react-native";

function ExtratoFinanceiro(props) {
  return (
    <View style={styles.container}>
      <View style={styles.rect5}>
        <Text style={styles.titulo}>Extrato Financeiro</Text>
        <View style={styles.item}>
          <Image
            source={require("../assets/images/ic22.png")}
            resizeMode="contain"
            style={styles.image}
          ></Image>
          <Text style={styles.atividadeConcluida}>Atividade 1 concluída</Text>
          <Text style={styles.r325}>- R$ 3,25</Text>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(219,213,213,1)"
  },
  rect5: {
    justifyContent: "flex-start",
    flex: 1
  },
  titulo: {
    height: 27,
    color: "rgba(53,52,52,1)",
    alignSelf: "stretch",
    justifyContent: "space-between",
    marginTop: 80,
    fontSize: 22,
    fontFamily: "roboto-regular",
    textAlign: "center"
  },
  item: {
    height: 45,
    backgroundColor: "rgba(231,226,226,1)",
    flexDirection: "row",
    alignItems: "center",
    alignSelf: "stretch",
    marginTop: 10,
    marginRight: 30,
    marginBottom: 10,
    marginLeft: 30,
    borderRadius: 5
  },
  image: {
    width: 27,
    height: 27,
    alignSelf: "center",
    marginLeft: 10
  },
  atividadeConcluida: {
    color: "rgba(14,14,14,1)",
    alignSelf: "center",
    marginLeft: 10,
    fontSize: 12,
    fontFamily: "roboto-regular"
  },
  r325: {
    flex: 1,
    color: "rgba(202,0,0,1)",
    alignSelf: "center",
    marginRight: 10,
    fontSize: 12,
    fontFamily: "roboto-regular",
    textAlign: "right"
  }
});

export default ExtratoFinanceiro;
