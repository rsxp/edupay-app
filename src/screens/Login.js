import React, { Component } from "react";
import { StyleSheet, View } from "react-native";
import MaterialButtonViolet from "../components/MaterialButtonViolet";

function Login(props) {
  return (
    <View style={styles.container}>
      <View style={styles.rect}>
        <MaterialButtonViolet
          style={styles.btnPai} labelText="Fluxo Pai"
        ></MaterialButtonViolet>
        <MaterialButtonViolet
          style={styles.btnFilho}  labelText="Fluxo Filho"
        ></MaterialButtonViolet>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(232,230,230,1)",
    justifyContent: "center"
  },
  rect: {
    height: 812,
    backgroundColor: "rgba(219,213,213,1)",
    justifyContent: "center"
  },
  
  btnPai: {
    height: 36,
    backgroundColor: "rgba(255,182,6,1)",
    alignSelf: "stretch",
    margin: 0,
    marginRight: 30,
    marginBottom: 32,
    marginLeft: 30,
    borderRadius: 5
  },
  
  btnFilho: {
    height: 36,
    backgroundColor: "rgba(255,182,6,1)",
    alignSelf: "stretch",
    margin: 0,
    marginRight: 30,
    marginBottom: 32,
    marginLeft: 30,
    borderRadius: 5
  }
});

export default Login;
