import React, { Component } from "react";
import { StyleSheet, View, Text, Image } from "react-native";
import MaterialButtonViolet1 from "../components/MaterialButtonViolet1";

function MeuSaldo(props) {
  return (
    <View style={styles.container}>
      <View style={styles.global}>
        <View style={styles.saldo}>
          <Text style={styles.titulo}>Meu saldo</Text>
          <Text style={styles.saldoTotal}>R$ 350,00</Text>
          <MaterialButtonViolet1
            style={styles.btnAddCreditos}
          ></MaterialButtonViolet1>
        </View>
        <View style={styles.links}>
          <View style={styles.rect}>
            <View style={styles.rect2}>
              <Image
                source={require("../assets/images/ic1.png")}
                resizeMode="contain"
                style={styles.icoBone}
              ></Image>
              <Text style={styles.progressoEscolar2}>Progresso escolar</Text>
            </View>
            <View style={styles.rect3}>
              <Image
                source={require("../assets/images/ic21.png")}
                resizeMode="contain"
                style={styles.icoMoney}
              ></Image>
              <Text style={styles.extratoFinanceiro}>Extrato Financeiro</Text>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(236,234,234,1)"
  },
  global: {
    backgroundColor: "rgba(219,213,213,1)",
    justifyContent: "flex-start",
    flex: 1
  },
  saldo: {
    height: 210,
    backgroundColor: "rgba(247,244,244,1)",
    alignSelf: "stretch",
    justifyContent: "flex-start",
    marginTop: 80,
    marginRight: 30,
    marginLeft: 30
  },
  titulo: {
    color: "rgba(53,52,52,1)",
    alignSelf: "stretch",
    marginTop: 15,
    fontSize: 22,
    fontFamily: "roboto-300",
    textAlign: "center"
  },
  saldoTotal: {
    color: "rgba(53,52,52,1)",
    alignSelf: "stretch",
    marginTop: 20,
    fontSize: 50,
    fontFamily: "roboto-regular",
    textAlign: "center"
  },
  btnAddCreditos: {
    height: 35,
    backgroundColor: "rgba(3,134,30,1)",
    alignSelf: "stretch",
    marginTop: 20,
    marginRight: 15,
    marginLeft: 15,
    borderRadius: 5
  },
  links: {
    height: 440,
    alignSelf: "stretch",
    justifyContent: "space-between",
    marginTop: 30,
    marginRight: 30,
    marginLeft: 30
  },
  rect: {
    height: 109,
    flexDirection: "row",
    marginTop: 19
  },
  rect2: {
    flex: 0.5,
    alignItems: "center",
    justifyContent: "flex-start"
  },
  icoBone: {
    width: 158,
    height: 44,
    marginTop: 10,
    marginRight: 30,
    marginBottom: 30,
    marginLeft: 30
  },
  progressoEscolar2: {
    color: "rgba(7,60,233,1)",
    fontFamily: "roboto-regular"
  },
  rect3: {
    flex: 0.5,
    alignItems: "center",
    justifyContent: "center"
  },
  icoMoney: {
    width: 158,
    height: 46,
    marginRight: 30,
    marginBottom: 30,
    marginLeft: 30
  },
  extratoFinanceiro: {
    color: "rgba(3,134,30,1)",
    fontFamily: "roboto-regular"
  }
});

export default MeuSaldo;
