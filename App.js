import React, { useState } from "react";
import { StackNavigator, DrawerNavigator } from "react-navigation";
import { AppLoading } from "expo";
import * as Font from "expo-font";
import ExtratoFinanceiro from "./src/screens/ExtratoFinanceiro";
import ExtratoEscolar from "./src/screens/ExtratoEscolar";
import MeuSaldo from "./src/screens/MeuSaldo";
import Login from "./src/screens/Login";

const DrawerNavigation = DrawerNavigator({
  Login: {
    screen: Login
  },
  MeuSaldo: {
    screen: MeuSaldo
  },
  ExtratoFinanceiro: {
    screen: ExtratoFinanceiro
  },
  ExtratoEscolar: {
    screen: ExtratoEscolar
  }
});

const StackNavigation = StackNavigator(
  {
    DrawerNavigation: {
      screen: DrawerNavigation
    },
    Login: {
      screen: Login
    },
    MeuSaldo: {
      screen: MeuSaldo
    },
    ExtratoFinanceiro: {
      screen: ExtratoFinanceiro
    },
    ExtratoEscolar: {
      screen: ExtratoEscolar
    },
  },
  {
    headerMode: "none"
  }
);

function App() {
  const [isLoadingComplete, setLoadingComplete] = useState(false);
  if (!isLoadingComplete) {
    return (
      <AppLoading
        startAsync={loadResourcesAsync}
        onError={handleLoadingError}
        onFinish={() => handleFinishLoading(setLoadingComplete)}
      />
    );
  } else {
    return isLoadingComplete ? <StackNavigation /> : <AppLoading />;
  }
}
async function loadResourcesAsync() {
  await Promise.all([
    Font.loadAsync({
      "roboto-700": require("./src/assets/fonts/roboto-700.ttf"),
      "roboto-regular": require("./src/assets/fonts/roboto-regular.ttf"),
      "roboto-300": require("./src/assets/fonts/roboto-300.ttf")
    })
  ]);
}
function handleLoadingError(error) {
  console.warn(error);
}

function handleFinishLoading(setLoadingComplete) {
  setLoadingComplete(true);
}

export default App;
